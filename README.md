## **WARNING: This repository is currently NOT UPDATED/MAINTAINED. To get the most recent version of MORE, please go to Github: https://github.com/ConesaLab/MORE ** ##

# MORE R package repository

## Installing

Currently the easiest option for installing the package is to use the devtools R package:

    install.packages("devtools")
    devtools::install_bitbucket("ConesaLab/more")

Before doing that it might be necessary to install the required dependencies:

* pbapply
* glmnet
* igraph
* MASS
* parallel
* psych
* car

## Usage

You can download the main vignette of the package from the download section in this repository: https://bitbucket.org/ConesaLab/more/downloads/

In that directory you will also find a zipped folder containing the required files to run the package as a Shiny application.

You can see a MORE Shiny application example in the user guide and also in the following video: https://youtu.be/SSIaeFRNsXg

